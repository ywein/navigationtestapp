import UIKit
import EddystoneScanner
import CoreMotion

struct BeaconStruct: Decodable {
    let uid: String
    let height: Float
    let x: Float
    let y: Float
    let type: String
    let n: Float
}

struct Location: Codable {
    let seconds: Double
    let id: String
    let tx: Int
    let rssi: Int
}

struct PostData: Codable {
    let dist: Double?
    let heading: Double?
    let ble: [Location]
    let id: String
}

struct ResponseData: Decodable {
    let label: String
    let color: String
    let data: Array<Float>
}

class ViewController: UIViewController, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.type = self.pickerData[row]
        self.fillBeacons()
        if (self.type == "lobby2") {
            let backgroundImage = UIImage(named: "floor2")
            self.background.image = backgroundImage
        } else {
            let backgroundImage = UIImage(named: "floor1")
            self.background.image = backgroundImage
        }
    }
    
    //MARK: Properties
    
    @IBAction func TapMark(_ gestureRecognizer : UITapGestureRecognizer) {
        guard gestureRecognizer.view != nil else { return }
        if gestureRecognizer.state == .ended {
            let location = gestureRecognizer.location(in: gestureRecognizer.view)
            
            self.marker?.backgroundColor = UIColor(red: 0.8, green: 0, blue: 0, alpha: 1)
            self.marker?.frame = CGRect(x: location.x, y: location.y, width:16, height:16)
        }
    }
    
    @IBAction func ImuSwitchAction(_ sender: UISwitch) {
        self.imuState = sender.isOn
    }
    @IBOutlet weak var ImuSwitch: UISwitch!
    @IBOutlet weak var ActivityTypeLabel: UILabel!
    @IBOutlet weak var Pace: UILabel!
    @IBOutlet weak var Compass: UILabel!
    @IBOutlet weak var Map: UIView!
    @IBOutlet weak var typesPicker: UIPickerView!
    @IBOutlet weak var MapScroll: UIScrollView!
    @IBOutlet weak var StatusText: UILabel!
    @IBAction func ReloadData(_ sender: Any) {
        self.getBeaconsData()
    }
    @IBOutlet weak var background: UIImageView!
    
    var beaconsArray: [BeaconStruct]!
    
    var locations: [Location] = []
    
    var pickerData: [String] = [String]()
    var coord: [Int]? = [0, 0]
    var type: String = "all"
    var myLocation: [UIView]?
    var swapXY: Bool = false
    var uuid: String = ""
    
    var rssiLimitValue: Int = -85
    
    let numberToolbar: UIToolbar = UIToolbar()
    
    var marker: UIView?
    
    let scanner = EddystoneScanner.Scanner()
    var timer : Timer?
    
    var beaconList = [Beacon]()
    
    let pedometer = CMPedometer()
    let motionMgr = CMMotionManager()
    let activityManager = CMMotionActivityManager()
    
    var totalDistance: Double = 0
    var distanceFromLast: Double = 0
    var distance: Double = 0
    var heading: [Double] = []
    var xAccel: Double = 0
    var yAccel: Double = 0
    var zAccel: Double = 0
    var xDist: Double = 0
    var yDist: Double = 0
    var zDist: Double = 0
    var distAccel: Double = 0
    var accelTime: Double = 0
    var timeTest: Double = 0
    var activityType = "stationary"
    var imuState: Bool = true
    var pedometerActive: Bool = true
    
    func startTrackingActivityType() {
        activityManager.startActivityUpdates(to: OperationQueue.main) {
            [weak self] (activity: CMMotionActivity?) in
            
            guard let activity = activity else { return }
            if (activity.walking || activity.running) {
                self?.activityType = "walking"
            } else {
                self?.xAccel = 0
                self?.yAccel = 0
                self?.zAccel = 0
                self?.accelTime = 0
                self?.activityType = "stationary"
            }
            DispatchQueue.main.async {
                if activity.walking {
                    self?.ActivityTypeLabel.text = "Walking"
                } else if activity.stationary {
                    self?.ActivityTypeLabel.text = "Stationary"
                } else if activity.running {
                    self?.ActivityTypeLabel.text = "Running"
                } else {
                    self?.ActivityTypeLabel.text = "Other"
                }
            }
        }
    }
    
//
//    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
//        self.heading.append(newHeading.trueHeading)
//        DispatchQueue.main.async {
//            self.Compass.text = "\(newHeading.trueHeading)" + " " + "\(newHeading.headingAccuracy)"
//        }
//    }
//
    
    override func viewDidLayoutSubviews() {
        self.MapScroll.contentSize = self.Map.frame.size
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return Map
    }
    
    func handleMove(motion: CMDeviceMotion?, error: Error?) {
        if (self.pedometerActive) {
            self.heading.append(motion?.heading ?? 0)
        } else {
            if (self.accelTime == 0) {
                self.accelTime = (motion?.timestamp)!
            } else if (self.activityType == "walking"){
                let timeInterval = (motion?.timestamp)! - self.accelTime
                self.accelTime = (motion?.timestamp)!
                self.xAccel += (((motion?.userAcceleration.x)! * 9.81)) * timeInterval
                self.yAccel += (((motion?.userAcceleration.y)! * 9.81)) * timeInterval
                self.zAccel += (((motion?.userAcceleration.z)! * 9.81)) * timeInterval
                
                self.xDist += self.xAccel * timeInterval
                self.yDist += self.yAccel * timeInterval
                self.zDist += self.zAccel * timeInterval
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.lm = CLLocationManager()
//        self.lm?.delegate = self
//        self.lm?.requestAlwaysAuthorization()
//        self.lm?.startUpdatingHeading()
        
        if (CMPedometer.isDistanceAvailable()) {
            self.pedometerActive = true
            self.pedometer.startUpdates(from: Date()) { (data: CMPedometerData?, error) -> Void in
                //            print(data)
                //            print(error)
                DispatchQueue.main.async(execute: { () -> Void in
                    if(error == nil){
                        //self.step.text = "\(data!.numberOfSteps)"
                        self.totalDistance = Double(truncating: data?.distance ?? 0)
                        self.distance = self.totalDistance - self.distanceFromLast
                        self.Pace.text = "\(data!.distance ?? 0)" + " " + "\(self.distance)"
                    }
                })
            }
        } else {
//            self.pedometerActive = false // ONLY FOR TESTS
        }
        
        if self.motionMgr.isDeviceMotionAvailable {
            self.motionMgr.showsDeviceMovementDisplay = true
            self.motionMgr.startDeviceMotionUpdates(using: .xMagneticNorthZVertical, to: OperationQueue(), withHandler: handleMove)
        }
        
        if CMMotionActivityManager.isActivityAvailable() {
            startTrackingActivityType()
        }
        
        scanner.startScanning()
        scanner.delegate = self
    
        self.MapScroll.delegate = self
        self.MapScroll.minimumZoomScale = 0.5
        self.MapScroll.maximumZoomScale = 4.0
        self.MapScroll.zoomScale = 1.0
        
        background.accessibilityIdentifier = "background"
        
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        DispatchQueue.main.async {
            self.StatusText.text = dateString + " Collecting data"
        }
        
        self.uuid = UUID().uuidString
        
        self.typesPicker.delegate = self
        self.typesPicker.dataSource = self
        self.pickerData = ["all", "ceiling", "hall", "lobby", "lobby2", "room"]
        
        DispatchQueue.main.async {
            let customView2 = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
            let color2 = UIColor(red: 0.8, green: 0, blue: 0, alpha: 0)
            customView2.backgroundColor = color2
            customView2.layer.cornerRadius = customView2.frame.size.width/2
            customView2.clipsToBounds = true
            customView2.accessibilityIdentifier = "marker"
            self.Map.addSubview(customView2)
            self.marker = customView2
        }
        
        self.getBeaconsData()
        self.scheduledTimerWithTimeInterval()
    }
    
    func getBeaconsData() {
        let beaconsUrl = URL(string: "https://navigationapi.webmind28.com/beacons")
        
        let task = URLSession.shared.dataTask(with: beaconsUrl!) {(data, response, error ) in
            guard error == nil else {
                print("returned error")
                return
            }
            guard let content = data else {
                print("No data")
                return
            }
            let decoder = JSONDecoder()
            let result = try! decoder.decode([BeaconStruct].self, from: content)
            self.beaconsArray = result
            self.fillBeacons()
        }
        
        task.resume()
    }
    
    func fillBeacons() {
        DispatchQueue.main.async {
            for view in self.Map.subviews {
                if (view.accessibilityIdentifier != "myLocation" && view.accessibilityIdentifier != "background" && view.accessibilityIdentifier != "marker") {
                    view.removeFromSuperview()
                }
            }
            for beacon in self.beaconsArray {
                if (self.type == "room" && self.type != beacon.type) {
                    continue
                } else if (self.type != "room" && beacon.type == "room") {
                    continue
                }
                
                if (self.type == "lobby2" && self.type != beacon.type) {
                    continue
                } else if (self.type != "lobby2" && beacon.type == "lobby2") {
                    continue
                }
                
                let y = self.type == "room" ? Int(beacon.y) * 10 : Int(beacon.y)
                let x = self.type == "room" ? Int(beacon.x) * 10 : Int(beacon.x)
                let customView = UIView(frame: CGRect(x: y, y: x, width: 10, height: 10))
                var alpha = 0.5
                //                NSLog("%@", self.type)
                if (self.type == beacon.type) {
                    alpha = 1.0
                }
                var color = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: CGFloat(alpha))
                if (beacon.type == "hall") {
                    color = UIColor(red: 242/255, green: 24/255, blue: 24/255, alpha: CGFloat(alpha))
                } else if (beacon.type == "lobby") {
                    color = UIColor(red: 23/255, green: 158/255, blue: 241/255, alpha: CGFloat(alpha))
                } else if (beacon.type == "lobby2") {
                    color = UIColor(red: 23/255, green: 158/255, blue: 241/255, alpha: CGFloat(alpha))
                } else if (beacon.type == "ceiling") {
                    color = UIColor(red: 43/255, green: 211/255, blue: 41/255, alpha: CGFloat(alpha))
                } else if (beacon.type == "room") {
                    color = UIColor(red: 206/255, green: 37/255, blue: 133/255, alpha: CGFloat(alpha))
                }
                
                customView.backgroundColor = color
                customView.layer.cornerRadius = customView.frame.size.width/2
                customView.clipsToBounds = true
                self.Map.addSubview(customView)
            }
        }
    }
    
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        var date = Date()
        var dateString = dateFormatter.string(from: date)
        DispatchQueue.main.async {
            if (self.distAccel > 0) {
                self.Pace.text = String(self.distAccel)
            }
            self.StatusText.text = dateString + " Sending data"
        }
        var filteredLocations: [Location] = []
        let coordinatesUrl = URL(string: "https://navigationapi.webmind28.com/coordinates2")

        if (self.type == "all") {
            filteredLocations = self.locations
        } else {
            filteredLocations = self.locations.filter({(location) -> Bool in
                return self.beaconsArray.contains(where: {$0.uid.lowercased() == location.id && $0.type == self.type})
            })
        }
        
        var filteredBeaconsList: [String] = filteredLocations.map({(location) -> String in
            return location.id
        })
        
        filteredBeaconsList = Array(Set(filteredBeaconsList))
        
        
        self.distAccel = sqrt(pow(0 - self.xDist, 2) + pow(0 - self.yDist, 2) + pow(0 - self.zDist, 2))
        
        if (self.imuState) {
            
        } else {
            self.distance = 0
            self.heading = []
        }
        
        var headingSum = 0.0
        if (self.heading.count > 0) {
            headingSum = self.heading.reduce(0, +) / Double(self.heading.count)
        }
        print(headingSum)
        let postData = PostData(dist: self.distance, heading: headingSum, ble: filteredLocations, id: self.uuid)
        
        DispatchQueue.main.async {
            self.Compass.text = "\(headingSum)"
        }

        guard let jsonData = try? JSONEncoder().encode(postData) else {
            return
        }
        //        NSLog("filtered %@", String(filteredLocations.count))
        //        print("filtered %@", String(data: jsonData, encoding: String.Encoding.utf8) as String!)
        
        if (filteredBeaconsList.count > 3) {
            self.locations = []
            self.distance = 0
            self.distanceFromLast = self.totalDistance
            self.heading = [headingSum]
            
            self.xDist = 0
            self.yDist = 0
            self.zDist = 0
            self.distAccel = 0
            
            var request = URLRequest(url: coordinatesUrl!)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=(String(describing: error))")
                    date = Date()
                    dateString = dateFormatter.string(from: date)
                    DispatchQueue.main.async {
                        self.StatusText.text = dateString + " Data send error"
                    }
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is (httpStatus.statusCode)")
                    print("response = (String(describing: response))")
                    date = Date()
                    dateString = dateFormatter.string(from: date)
                    DispatchQueue.main.async {
                        self.StatusText.text = dateString + " Data send error"
                    }
                }
                
                guard let jsonData = try? JSONDecoder().decode(Array<ResponseData>.self, from: data) else {
                    date = Date()
                    dateString = dateFormatter.string(from: date)
                    DispatchQueue.main.async {
                        self.StatusText.text = dateString + " Data send error"
                    }
                    return
                }
//                NSLog("%@", jsonData)
//
                
                date = Date()
                dateString = dateFormatter.string(from: date)
                DispatchQueue.main.async {
                    self.StatusText.text = dateString + " Collecting ble data"
                }
                
                DispatchQueue.main.async {
                    for view in self.Map.subviews {
                        if (view.accessibilityIdentifier == "myLocation") {
                            view.removeFromSuperview()
                        }
                    }
                    for item in jsonData {
                        let customView = self.createLocationView(pos: item.data, color: item.color)
                        self.Map.addSubview(customView)
                    }
                }
            }
            task.resume()
        } else {
            //            NSLog("%@", "NOT ENOUGH DATA")
            date = Date()
            dateString = dateFormatter.string(from: date)
            DispatchQueue.main.async {
                self.StatusText.text = dateString + " Not enough beacons data 2"
            }
        }
    }
    
    func createLocationView(pos: [Float], color: String) -> UIView {
        let y = self.type == "room" ? Int(pos[1]) * 10 : Int(pos[1])
        let x = self.type == "room" ? Int(pos[0]) * 10 : Int(pos[0])

        let customView = UIView(frame: CGRect(x: x, y: y, width: 16, height: 16))
        let color = UIColor(hexString: color)
        customView.backgroundColor = color
        customView.layer.cornerRadius = customView.frame.size.width/2
        customView.clipsToBounds = true
        customView.accessibilityIdentifier = "myLocation"
//        self.Map.addSubview(customView)
        return customView
    }
//
//    func didFindBeacon(beaconScanner: BeaconScanner, beaconInfo: BeaconInfo) {
//        //    NSLog("FIND: %@", beaconInfo.description)
//        //    if (beaconInfo.RSSI > self.rssiLimitValue && beaconInfo.RSSI < 0) {
//        //        locations.append(Location(seconds: Int(NSDate().timeIntervalSince1970), id: beaconInfo.beaconID.hexID, tx: beaconInfo.txPower, rssi: beaconInfo.RSSI))
//        //    }
//        if (beaconInfo.RSSI < 0) {
//            locations.append(Location(seconds: Int(NSDate().timeIntervalSince1970), id: beaconInfo.beaconID.hexID, tx: beaconInfo.txPower, rssi: beaconInfo.RSSI))
//        }
//    }
//    func didLoseBeacon(beaconScanner: BeaconScanner, beaconInfo: BeaconInfo) {
//        //    NSLog("LOST: %@", beaconInfo.description)
//    }
//    func didUpdateBeacon(beaconScanner: BeaconScanner, beaconInfo: BeaconInfo) {
//        //    if (beaconInfo.RSSI > self.rssiLimitValue && beaconInfo.RSSI < 0) {
//        //        locations.append(Location(seconds: Int(NSDate().timeIntervalSince1970), id: beaconInfo.beaconID.hexID, tx: beaconInfo.txPower, rssi: beaconInfo.RSSI))
//        ////            NSLog("UPDATE: %@", beaconInfo.description)
//        //    }
//        if (beaconInfo.RSSI < 0) {
//            locations.append(Location(seconds: Int(NSDate().timeIntervalSince1970), id: beaconInfo.beaconID.hexID, tx: beaconInfo.txPower, rssi: beaconInfo.RSSI))
//        }
//    }
//    func didObserveURLBeacon(beaconScanner: BeaconScanner, URL: NSURL, RSSI: Int) {
//        //    NSLog("URL SEEN: %@, RSSI: %d", URL, RSSI)
//    }
    
}


extension ViewController: ScannerDelegate {
    
    // MARK: EddystoneScannerDelegate callbacks
    func didFindBeacon(scanner: EddystoneScanner.Scanner, beacon: Beacon) {
        if (beacon.filteredRSSI < 0) {
            beaconList.append(beacon)
            locations.append(Location(seconds: NSDate().timeIntervalSince1970, id: beacon.beaconID.description.lowercased(), tx: beacon.txPower, rssi: beacon.filteredRSSI))
        }
    }
    
    func didLoseBeacon(scanner: EddystoneScanner.Scanner, beacon: Beacon) {
        guard let index = beaconList.index(of: beacon) else {
            return
        }
        beaconList.remove(at: index)

    }
    
    func didUpdateBeacon(scanner: EddystoneScanner.Scanner, beacon: Beacon) {
        guard let index = beaconList.index(of: beacon) else {
            if (beacon.filteredRSSI < 0) {
                beaconList.append(beacon)
            }
            return
        }
        if (beacon.filteredRSSI < 0) {
            beaconList[index] = beacon
            locations.append(Location(seconds: NSDate().timeIntervalSince1970, id: beacon.beaconID.description.lowercased(), tx: beacon.txPower, rssi: beacon.filteredRSSI))
        }
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
